COPY toolchains /duneci/toolchains
COPY cmake-flags /duneci/cmake-flags
COPY dune.opts /duneci/
RUN chown duneci /duneci/cmake-flags
USER duneci
WORKDIR /duneci
RUN mkdir -p /duneci/bin /duneci/modules
COPY duneci-install-module duneci-standard-test dune-ctest duneci-init-job /duneci/bin/
ENV DUNE_CONTROL_PATH=.:/duneci/modules
ENV PATH=/duneci/bin:$PATH
RUN ln -s dune-ctest /duneci/bin/duneci-ctest
