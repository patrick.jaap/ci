Docker images for GitLab CI
===========================

Images
------

The current images are (to keep the list legible, `rdo/` = `registry.dune-project.org/`:

### Service Images

| image                               | parent        | mirror | description                                    |
|-------------------------------------|---------------|--------|------------------------------------------------|
| rdo/docker/ci/service/docker:latest | alpine:latest | ---    | Utility image for building other Docker images |

### Base Images

The base images all offer a set of _toolchains_. You can select a toolchain by defining the job variable `DUNECI_TOOLCHAIN`. The
corresponding opts file is always `/duneci/dune.opts`, but that will be automatically picked up by the scripts in the image. Toolchains are
given by strings of the following format: `<compiler>-<version>-<optional extras>-<c++ standard>`.

| image                      | parent       | mirror              | description                        | toolchains                                                                                                                                         |
|----------------------------|--------------|---------------------|------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------|
| rdo/docker/ci/debian:9     | debian:9     | duneci/debian:9     | Debian 9 with cmake 3.7            | gcc-6-14, gcc-6-noassert-14, clang-3.8-14, clang-3.8-noassert-14                                                                                   |
| rdo/docker/ci/debian:10    | debian:10    | duneci/debian:10    | Debian 10 with cmake 3.11          | gcc-7-14, gcc-7-noassert-14, gcc-7-17, gcc-8-17, gcc-8-noassert-17, clang-6-17, clang-6-noassert-17 clang-7-libcpp-17, clang-7-libcpp-noassert-17 |
| rdo/docker/ci/ubuntu:16.04 | ubuntu:16.04 | duneci/ubuntu:16.04 | Ubuntu LTS 16.04 with cmake 3.5.1  | gcc-5-14, gcc-5-noassert-14, clang-3.8-14                                                                                                          |
| rdo/docker/ci/ubuntu:18.04 | ubuntu:18.04 | duneci/ubuntu:18.04 | Ubuntu LTS 18.04 with cmake 3.10.2 | gcc-7-14, gcc-7-noassert-14, gcc-7-17, gcc-7-noassert-17, clang-6-17, clang-6-noassert-17                                                          |

Debian 10 uses GCC 8 as the default compiler, but we still default to GCC 7 for now.

### Core Images

These images contain the core and staging modules compiled for several combinations of distributions and toolchains. The toolchain is locked
in for these images.

| image                                              | mirror                                      | description                                        |
|----------------------------------------------------|---------------------------------------------|----------------------------------------------------|
| rdo/docker/ci/dune:2.4-ubuntu-16.04-gcc-5-14       | duneci/dune:2.4-ubuntu-16.04-gcc-5-14       | DUNE 2.4 core modules (Debian packages)            |
| rdo/docker/ci/dune:2.5-debian-9-gcc-6-14           | duneci/dune:2.5-debian-9-gcc-6-14           | DUNE 2.5 core and staging modules (Git 2.5 branch) |
| rdo/docker/ci/dune:2.6-debian-9-gcc-6-14           | duneci/dune:2.6-debian-9-gcc-6-14           | DUNE 2.6 core and staging modules (Git 2.6 branch) |
| rdo/docker/ci/dune:2.6-debian-10-gcc-7-14          | duneci/dune:2.6-debian-10-gcc-7-14          | DUNE 2.6 core and staging modules (Git 2.6 branch) |
| rdo/docker/ci/dune:2.6-debian-10-gcc-7-noassert-14 | duneci/dune:2.6-debian-10-gcc-7-noassert-14 | DUNE 2.6 core and staging modules (Git 2.6 branch) |
| rdo/docker/ci/dune:2.6-debian-10-gcc-8-17          | duneci/dune:2.6-debian-10-gcc-8-17          | DUNE 2.6 core and staging modules (Git 2.6 branch) |
| rdo/docker/ci/dune:2.6-debian-10-clang-6-libcpp-17 | duneci/dune:2.6-debian-10-clang-6-libcpp-17 | DUNE 2.6 core and staging modules (Git 2.6 branch) |
| rdo/docker/ci/dune:2.6-ubuntu-18.04-clang-6-17     | duneci/dune:2.6-ubuntu-18.04-clang-6-17     | DUNE 2.6 core and staging modules (Git 2.6 branch) |
| rdo/docker/ci/dune:git-debian-9-gcc-6-14           | duneci/dune:git-debian-9-gcc-6-14           | DUNE master core and staging modules (Git)         |
| rdo/docker/ci/dune:git-debian-10-gcc-7-14          | duneci/dune:git-debian-10-gcc-7-14          | DUNE master core and staging modules (Git)         |
| rdo/docker/ci/dune:git-debian-10-gcc-8-17          | duneci/dune:git-debian-10-gcc-8-17          | DUNE master core and staging modules (Git)         |
| rdo/docker/ci/dune:git-debian-10-gcc-8-noassert-17 | duneci/dune:git-debian-10-gcc-8-noassert-17 | DUNE master core and staging modules (Git)         |
| rdo/docker/ci/dune:git-debian-10-clang-6-libcpp-17 | duneci/dune:git-debian-10-clang-7-libcpp-17 | DUNE master core and staging modules (Git)         |
| rdo/docker/ci/dune:git-ubuntu-18.04-clang-6-17     | duneci/dune:git-ubuntu-18.04-clang-6-17     | DUNE master core and staging modules (Git)         |


There is also a predefined image for each version:

| image                  | distribution | toolchain |
|------------------------|--------------|-----------|
| rdo/docker/ci/dune:2.4 | ubuntu-16.04 | gcc-5-14  |
| rdo/docker/ci/dune:2.5 | debian-9     | gcc-6-14  |
| rdo/docker/ci/dune:2.6 | debian-10    | gcc-7-14  |
| rdo/docker/ci/dune:git | debian-10    | gcc-7-14  |


### Module Dependency Images

These images contain all dependencies for testing the respective downstream module. They are mirrored in a similar way to the images listed above.

| image                                                          | parent                                             | description                                                |
|----------------------------------------------------------------|----------------------------------------------------|------------------------------------------------------------|
| rdo/docker/ci/dune-pdelab-deps:2.6-debian-9-gcc-6-14           | rdo/docker/ci/dune:2.6-debian-9-gcc-6-14           | dune-alugrid (2.6 branch)                                  |
| rdo/docker/ci/dune-pdelab-deps:2.6-debian-10-gcc-7-noassert-14 | rdo/docker/ci/dune:2.6-debian-10-gcc-7-noassert-14 | dune-alugrid (2.6 branch)                                  |
| rdo/docker/ci/dune-pdelab-deps:2.6-debian-10-gcc-8-17          | rdo/docker/ci/dune:2.6-debian-10-gcc-8-17          | dune-alugrid (2.6 branch)                                  |
| rdo/docker/ci/dune-pdelab-deps:2.6-debian-10-clang-6-libcpp-17 | rdo/docker/ci/dune:2.6-debian-10-clang-7-libcpp-17 | dune-alugrid (2.6 branch)                                  |
| rdo/docker/ci/dune-pdelab-deps:git-debian-10-gcc-8-17          | rdo/docker/ci/dune:git-debian-10-gcc-8-17          | dune-alugrid (master branch), dune-logging (master branch) |
| rdo/docker/ci/dune-pdelab-deps:git-debian-10-gcc-8-noassert-17 | rdo/docker/ci/dune:git-debian-10-gcc-8-noassert-17 | dune-alugrid (master branch), dune-logging (master branch) |
| rdo/docker/ci/dune-pdelab-deps:git-debian-10-clang-6-libcpp-17 | rdo/docker/ci/dune:git-debian-10-clang-6-libcpp-17 | dune-alugrid (master branch), dune-logging (master branch) |


### Module Images

These images are for testing downstream modules.

| image                                                     | parent                                                         | description                                                        |
|-----------------------------------------------------------|----------------------------------------------------------------|--------------------------------------------------------------------|
| rdo/docker/ci/dune-fufem:2.4                              | rdo/docker/ci/dune:2.4                                         | dune-{fufem,functions,solvers,typetree} (2.4 branch)               |
| rdo/docker/ci/dune-fufem:git                              | rdo/docker/ci/dune:git                                         | dune-{elasticity,fufem,grid-glue,solvers} (master branch)          |
| rdo/docker/ci/dune-pdelab:2.6-debian-9-gcc-6-14           | rdo/docker/ci/dune-pdelab-deps:2.6-debian-9-gcc-6-14           | dune-{pdelab,testtools} (2.6 branch), enabled Python virtualenv    |
| rdo/docker/ci/dune-pdelab:2.6-debian-10-gcc-7-noassert-14 | rdo/docker/ci/dune-pdelab-deps:2.6-debian-10-gcc-7-noassert-14 | dune-{pdelab,testtools} (2.6 branch), enabled Python virtualenv    |
| rdo/docker/ci/dune-pdelab:2.6-debian-10-gcc-8-17          | rdo/docker/ci/dune-pdelab-deps:2.6-debian-10-gcc-8-17          | dune-{pdelab,testtools} (2.6 branch), enabled Python virtualenv    |
| rdo/docker/ci/dune-pdelab:2.6-debian-10-clang-6-libcpp-17 | rdo/docker/ci/dune-pdelab-deps:2.6-debian-10-clang-7-libcpp-17 | dune-{pdelab,testtools} (2.6 branch), enabled Python virtualenv    |
| rdo/docker/ci/dune-pdelab:git-debian-10-gcc-8-17          | rdo/docker/ci/dune-pdelab-deps:git-debian-10-gcc-8-17          | dune-{pdelab,testtools} (master branch), enabled Python virtualenv |
| rdo/docker/ci/dune-pdelab:git-debian-10-gcc-8-noassert-17 | rdo/docker/ci/dune-pdelab-deps:git-debian-10-gcc-8-noassert-17 | dune-{pdelab,testtools} (master branch), enabled Python virtualenv |
| rdo/docker/ci/dune-pdelab:git-debian-10-clang-6-libcpp-17 | rdo/docker/ci/dune-pdelab-deps:git-debian-10-clang-7-libcpp-17 | dune-{pdelab,testtools} (master branch), enabled Python virtualenv |


`.gitlab-ci.yml`
----------------

Installing dependencies:
```yaml
before_script:
  - duneci-install-module https://gitlab.dune-project.org/core/dune-common.git
  - duneci-install-module https://gitlab.dune-project.org/core/dune-geometry.git
```

To build with several images:
```yaml
---
dune:2.4--gcc:
  image: registry.dune-project.org/docker/ci/dune:2.6
  script: duneci-standard-test

dune:2.4--clang:
  image: registry.dune-project.org/docker/ci/dune:2.6
  script: duneci-standard-test --opts=/duneci/opts.clang
```

You can also specify a default image and use it in several jobs:

```yaml
---
image: registry.dune-project.org/docker/ci/dune:2.6

dune:2.4--gcc:
  script: duneci-standard-test

dune:2.4--clang:
  script: duneci-standard-test --opts=/duneci/opts.clang
```

For more information, take a look at the [GitLab documentation on `.gitlab-ci.yml`](https://docs.gitlab.com/ce/ci/yaml/README.html).


Adding new images
-----------------

In order to add a new image, create a directory with the Dockerfile and any additional files that you want to have available in the Docker
build context. The naming scheme of the directory has to be `<image name>-<tag>`. Then edit `.gitlab-ci.yml` and add an appropriate entry to
the list of images in the correct stage (base / core / modules). If you want to have the image mirrored to [Docker Hub](https://hub.docker.com/),
talk to Ansgar or Steffen; they have to create the repository on the hub before that works.
